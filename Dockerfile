FROM gradle:6.8-jre11

ADD src src
ADD build.gradle.kts .
ADD settings.gradle.kts .

RUN gradle build

FROM openjdk:14-jdk-alpine

COPY build/libs/cashback-api-0.0.1-SNAPSHOT.jar cashback-api.jar

EXPOSE 8080

ENTRYPOINT exec java $JAVA_OPTS -jar /cashback-api.jar
