package br.com.grupo.boticario.cashbackapi.controller.advice

import br.com.grupo.boticario.cashbackapi.dto.error.ErrorResponseDTO
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class ControllerAdviceHandler: ResponseEntityExceptionHandler() {

    @ExceptionHandler(value = [(Exception::class)])
    fun handle(exception: Exception, request: WebRequest): ResponseEntity<ErrorResponseDTO>{
        return ResponseEntity(ErrorResponseDTO(exception.message!!, 400), HttpStatus.BAD_REQUEST)
    }

    @ExceptionHandler(value = [(NullPointerException::class)])
    fun handle(exception: NullPointerException, request: WebRequest): ResponseEntity<ErrorResponseDTO>{
        return ResponseEntity(ErrorResponseDTO("Objeto nulo ${exception.stackTrace}", 400), HttpStatus.BAD_REQUEST)
    }
}
