package br.com.grupo.boticario.cashbackapi.model

import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDateTime

@Document(collection = "reseller")
data class ResellerEntity(
    @Id val id: String? = ObjectId().toHexString(),
    val name: String,
    val document: String,
    val email: String,
    val password: String,
    val inclusionDateTime: LocalDateTime? = LocalDateTime.now()
) {
    constructor(name: String, document: String, email: String, password: String) :
            this(ObjectId().toHexString(), name, document, email, password, LocalDateTime.now())
}
