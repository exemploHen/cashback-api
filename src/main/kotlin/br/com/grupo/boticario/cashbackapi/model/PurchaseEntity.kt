package br.com.grupo.boticario.cashbackapi.model

import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.math.BigDecimal
import java.time.LocalDateTime

@Document(collection = "purchase")
data class PurchaseEntity(
    @Id val id: String? = ObjectId().toHexString(),
    val totalAmount: BigDecimal,
    val document: String,
    val cashbackPercentage: Int,
    val cashbackValue: BigDecimal,
    val purchaseDateTime: LocalDateTime? = LocalDateTime.now(),
    val status: String
) {
    constructor(totalAmount: BigDecimal, document: String, cashbackPercentage: Int, cashbackValue: BigDecimal) :
            this(ObjectId().toHexString(), totalAmount, document, cashbackPercentage, cashbackValue,
                LocalDateTime.now(),
                when (document) {
                    "15350946056" -> "Aprovado"
                    else -> "Em validação"
                }
            )
}
