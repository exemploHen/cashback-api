package br.com.grupo.boticario.cashbackapi.dto.external

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.io.Serializable
import java.math.BigDecimal

@JsonIgnoreProperties(ignoreUnknown = true)
class CumulativeCashbackResponseDTO(
    val body: Body,
    val statusCode: Int
) : Serializable

@JsonIgnoreProperties(ignoreUnknown = true)
class Body(
    val message: String?,
    val credit: BigDecimal?
) : Serializable
