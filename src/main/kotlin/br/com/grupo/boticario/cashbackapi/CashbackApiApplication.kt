package br.com.grupo.boticario.cashbackapi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CashbackApiApplication

fun main(args: Array<String>) {
    runApplication<CashbackApiApplication>(*args)
}
