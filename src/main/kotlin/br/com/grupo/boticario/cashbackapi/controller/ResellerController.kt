package br.com.grupo.boticario.cashbackapi.controller

import br.com.grupo.boticario.cashbackapi.dto.LoginDTO
import br.com.grupo.boticario.cashbackapi.dto.ResellerDTO
import br.com.grupo.boticario.cashbackapi.service.ResellerService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/reseller")
class ResellerController(private val resellerService: ResellerService) {

    @GetMapping("/list")
    fun resellersList() = ResponseEntity.ok(resellerService.findAllResellers())

    @GetMapping("/{document}")
    fun resellerByDocument(@PathVariable("document") document: String) = ResponseEntity.ok(resellerService.findResellerByDocument(document))

    @PostMapping("/new")
    fun createNewReseller(@RequestBody resellerDTO: ResellerDTO) = ResponseEntity.ok(resellerService.saveNewEntity(resellerDTO))

    @PostMapping("/login")
    fun login(@RequestBody loginDTO: LoginDTO) = ResponseEntity.ok(resellerService.login(loginDTO))

}
