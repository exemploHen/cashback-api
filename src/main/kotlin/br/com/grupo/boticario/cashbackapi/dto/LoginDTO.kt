package br.com.grupo.boticario.cashbackapi.dto

class LoginDTO(val document: String, val password: String)
