package br.com.grupo.boticario.cashbackapi.dto

import java.math.BigDecimal
import java.time.LocalDateTime

class PurchaseDTO(
    val id: String?,
    val totalAmount: BigDecimal,
    val document: String,
    val cashbackPercentage: Int,
    val cashbackValue: BigDecimal,
    val purchaseDateTime: LocalDateTime?,
    val reseller: ResellerDTO?,
    val status: String?)
