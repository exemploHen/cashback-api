package br.com.grupo.boticario.cashbackapi.dto.error

class ErrorResponseDTO(val message: String, val httpCode: Int) {
}
