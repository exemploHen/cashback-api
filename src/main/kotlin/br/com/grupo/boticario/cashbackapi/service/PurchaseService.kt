package br.com.grupo.boticario.cashbackapi.service

import br.com.grupo.boticario.cashbackapi.dto.PurchaseDTO
import br.com.grupo.boticario.cashbackapi.dto.ResellerDTO
import br.com.grupo.boticario.cashbackapi.dto.external.CumulativeCashbackResponseDTO
import br.com.grupo.boticario.cashbackapi.model.PurchaseEntity
import br.com.grupo.boticario.cashbackapi.repository.PurchaseRepository
import br.com.grupo.boticario.cashbackapi.service.external.CashbackBalancePartner
import org.springframework.stereotype.Service

@Service
class PurchaseService(private val purchaseRepository: PurchaseRepository,
                      private val cashbackBalancePartner: CashbackBalancePartner,
                      private val resellerService: ResellerService) {

    fun getCumulativeCashbackByDocument(document: String): CumulativeCashbackResponseDTO? =
        cashbackBalancePartner.getCumulativeCashbackBalance(document)

    fun getAllPurchases(): List<PurchaseDTO> =
        purchaseRepository.findAll().map<PurchaseEntity, PurchaseDTO> { entityToDTO(it) }.toList()

    fun saveNewEntity(purchaseDTO: PurchaseDTO): PurchaseDTO {
        val reseller = resellerService.findResellerByDocument(purchaseDTO.document)
        return entityToDTO(purchaseRepository.save(dtoToEntity(purchaseDTO, reseller)))
    }

    private fun entityToDTO(entity: PurchaseEntity): PurchaseDTO =
        PurchaseDTO(entity.id, entity.totalAmount, entity.document, entity.cashbackPercentage, entity.cashbackValue,
            entity.purchaseDateTime, resellerService.findResellerByDocument(entity.document), entity.status)

    private fun dtoToEntity(dto: PurchaseDTO, resellerDTO: ResellerDTO) =
        PurchaseEntity(dto.totalAmount, resellerDTO.document, dto.cashbackPercentage, dto.cashbackValue)
}
