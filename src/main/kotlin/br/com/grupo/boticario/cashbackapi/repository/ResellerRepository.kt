package br.com.grupo.boticario.cashbackapi.repository

import br.com.grupo.boticario.cashbackapi.model.ResellerEntity
import org.springframework.data.mongodb.repository.MongoRepository
import java.util.*

interface ResellerRepository : MongoRepository<ResellerEntity, String> {

    fun findByDocument(document: String): Optional<ResellerEntity>
}
