package br.com.grupo.boticario.cashbackapi.service

import br.com.grupo.boticario.cashbackapi.dto.LoginDTO
import br.com.grupo.boticario.cashbackapi.dto.ResellerDTO
import br.com.grupo.boticario.cashbackapi.model.ResellerEntity
import br.com.grupo.boticario.cashbackapi.repository.ResellerRepository
import org.springframework.stereotype.Service
import kotlin.streams.toList

@Service
class ResellerService(private val resellerRepository: ResellerRepository) {

    fun findResellerByDocument(document: String): ResellerDTO =
        entityToDTO(resellerRepository.findByDocument(document).orElseThrow{Exception("Revendedor não encontrado!")})

    fun saveNewEntity(dto: ResellerDTO): ResellerEntity {
        resellerRepository.findByDocument(dto.document)
            .takeIf { it.isEmpty } ?: throw Exception("Revendedor já cadastrado!")
        return resellerRepository.save(dtoToEntity(dto))
    }

    fun login(dto: LoginDTO): ResellerEntity {
        val loginAttempt = resellerRepository.findByDocument(dto.document)
            .takeIf { !it.isEmpty } ?: throw Exception("Verifique seu login e senha!")
        when (loginAttempt.get().password) {
            dto.password -> return resellerRepository.findByDocument(dto.document).get()
            else -> throw Exception("Verifique seu login e senha!")
        }
    }


    fun findAllResellers(): List<ResellerDTO> =
        resellerRepository.findAll().stream().map { entityToDTO(it) }.toList()

    private fun entityToDTO(entity: ResellerEntity): ResellerDTO =
        ResellerDTO(entity.name, entity.document, entity.email, entity.password)

    private fun dtoToEntity(dto: ResellerDTO): ResellerEntity =
        ResellerEntity(dto.name, dto.document, dto.email, dto.password.orEmpty())
}
