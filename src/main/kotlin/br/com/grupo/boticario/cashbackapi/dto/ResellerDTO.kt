package br.com.grupo.boticario.cashbackapi.dto

class ResellerDTO(val name: String, val document: String, val email: String, val password: String?)
