package br.com.grupo.boticario.cashbackapi.repository

import br.com.grupo.boticario.cashbackapi.model.PurchaseEntity
import org.springframework.data.mongodb.repository.MongoRepository

interface PurchaseRepository : MongoRepository<PurchaseEntity, String>
