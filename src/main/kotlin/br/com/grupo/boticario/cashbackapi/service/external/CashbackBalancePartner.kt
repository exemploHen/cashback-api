package br.com.grupo.boticario.cashbackapi.service.external

import br.com.grupo.boticario.cashbackapi.dto.external.CumulativeCashbackResponseDTO
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.exchange
import org.springframework.web.util.UriComponentsBuilder

@Service
class CashbackBalancePartner(@Value("\${external.cumulative-cashback.url}") val url: String,
                             @Value("\${external.cumulative-cashback.token}") val token: String,
                             val restTemplate: RestTemplate) {

    fun getCumulativeCashbackBalance(document: String): CumulativeCashbackResponseDTO? {
        val uriComponent = UriComponentsBuilder.fromHttpUrl(url).queryParam("cpf", document).build()
        val response: ResponseEntity<CumulativeCashbackResponseDTO> =
            restTemplate.exchange(uriComponent.toUriString(), HttpMethod.GET, buildParamAndHeaders(),
                CumulativeCashbackResponseDTO::class)
        return response.body
    }

    private fun buildParamAndHeaders(): HttpEntity<HashMap<String, String>> {
        val httpHeaders = HttpHeaders()
        httpHeaders.set("token", token)

        return HttpEntity(httpHeaders)
    }
}
