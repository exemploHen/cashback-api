package br.com.grupo.boticario.cashbackapi.controller

import br.com.grupo.boticario.cashbackapi.dto.PurchaseDTO
import br.com.grupo.boticario.cashbackapi.service.PurchaseService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/purchase")
class PurchaseController(private val purchaseService: PurchaseService) {

    @GetMapping("/list")
    fun resellersList(): ResponseEntity<List<PurchaseDTO>> =
        ResponseEntity.ok(purchaseService.getAllPurchases())

    @GetMapping("/{document}")
    fun cumulativeCashbackByDocument(@PathVariable("document") document: String) =
        ResponseEntity.ok(purchaseService.getCumulativeCashbackByDocument(document))

    @PostMapping("/new")
    fun createNewReseller(@RequestBody purchaseDTO: PurchaseDTO) =
        ResponseEntity.ok(purchaseService.saveNewEntity(purchaseDTO))

}
